package factoryPattern;

public class CuttonCandy extends Candy{

	public CuttonCandy(double price){
		//super(price, 0);
		this(price,0);          // either works
		
	}
	public CuttonCandy(double price, int sugarAmount) {
		super(price, sugarAmount);
	}
	@Override
	public void consume() {
		System.out.println("tearing cutton candy, eating cutton candy");
	}
	public String toString(){
		return "$"+this.getPrice()+" Cutton Candy contains "+this.getSugarAmount()+"kg of sugar!";
	}
	public static void main(String args[]){
		Candy mycandy = new CuttonCandy(10.0);
		System.out.println(mycandy.toString());
		mycandy.consume();
		CuttonCandy myCcandy = new CuttonCandy(11.5, 10);
		System.out.println(myCcandy.toString());
		myCcandy.consume();
	}
}
