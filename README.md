A factory pattern is a pattern that uses factory methods to handle the creation of an object without having to specifically naming the exact class that will be created. 
