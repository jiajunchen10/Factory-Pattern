package factoryPattern;

public abstract class Candy {
	private double price;
	private int sugarAmount;
	public Candy(double price, int sugarAmount){
		this.price = price;
		this.sugarAmount = sugarAmount;
	}
	public int getSugarAmount(){
		return this.sugarAmount;
	}
	public abstract void consume();
	public double getPrice(){
		return price;
	}
}
