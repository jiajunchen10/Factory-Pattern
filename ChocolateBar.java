package factoryPattern;

public class ChocolateBar extends Candy{
	private int Caffein;
	public ChocolateBar(double price){
		this(price, 0);
	}
	public ChocolateBar(double price, int sugarAmount) {
		this(price, sugarAmount, 0);
	}
	public ChocolateBar(double price, int sugarAmount, int Caffein){
		super(price, sugarAmount);
		this.Caffein = Caffein;
	}
	public int getCaffein(){
		return Caffein;
	}
	@Override
	public void consume() {
		System.out.println("biting and chewing chocolate bar");
	}
	public String toString(){
		return "$"+this.getPrice()+" chocolate bar contains "+this.getSugarAmount()+"kg of"
				+ " sugar and "+this.getCaffein()+"g of caffein";
	}
	public static void main(String args[]){
		Candy Bar1 = new ChocolateBar(20);
		Candy Bar2 = new ChocolateBar(30,10);
		ChocolateBar Bar3 = new ChocolateBar(40,20,10);
		System.out.println(Bar1.toString());
		Bar1.consume();
		System.out.println(Bar2.toString());
		Bar2.consume();
		System.out.println(Bar3.toString());
		Bar3.consume();
	}
}
