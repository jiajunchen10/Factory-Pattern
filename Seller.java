package factoryPattern;

public class Seller {

	public static void main(String[] args) {
		Candy candy1 = CandyStore.getCandy("chocolatebar");
		Candy candy2 = CandyStore.getCandy("cuttoncandy");
		Candy candy3 = CandyStore.getCandy("candy");
		
		try{
			System.out.println(candy1.toString());
			candy1.consume();
			System.out.println(candy2.toString());
			candy2.consume();
			System.out.println(candy3.toString());
			candy3.consume();
		}catch(NullPointerException e){
			System.out.println("got an empty candy");
		}
	}

}
