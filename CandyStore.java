package factoryPattern;

public class CandyStore {
	private CandyStore(){
		
	}
	public static Candy getCandy(String candyName){
		switch(candyName.trim().toLowerCase()){
			case "chocolatebar":
				return new ChocolateBar(20,10,10);
			case "cuttoncandy":
				return new CuttonCandy(10,10);
			case "candy":
				return new Candy(5,5){
					@Override
					public void consume() {
						System.out.println("eating candy");
					}
				};
			default:
				return null;
		}
	}
}
